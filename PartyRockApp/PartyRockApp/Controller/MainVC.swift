//
//  ViewController.swift
//  PartyRockApp
//
//  Created by Mark Davidson on 28/8/17.
//  Copyright © 2017 Mark Davidson. All rights reserved.
//
import UIKit

class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var partyRocks = [PartyRock]();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let p1 = PartyRock(imageURL: "http://lorempixel.com/100/100/people/1/", videoURL: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/x9oe6SpOLuY\" frameborder=\"0\" allowfullscreen></iframe>", videoTitle: "Tundra")
        
        let p2 = PartyRock(imageURL: "http://lorempixel.com/100/100/people/2/", videoURL: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/YZl0Vr7mgeY\" frameborder=\"0\" allowfullscreen></iframe>", videoTitle: "Great Northern")
        
        let p3 = PartyRock(imageURL: "http://lorempixel.com/100/100/people/3/", videoURL: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/AZSL_7W5Rqw\" frameborder=\"0\" allowfullscreen></iframe>", videoTitle: "Teach me")
        
        let p4 = PartyRock(imageURL: "http://lorempixel.com/100/100/people/4/", videoURL: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/1rfSHisyHdc\" frameborder=\"0\" allowfullscreen></iframe>", videoTitle: "Castle")
        
        let p5 = PartyRock(imageURL: "http://lorempixel.com/100/100/people/5/", videoURL: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/JGulAZnnTKA\" frameborder=\"0\" allowfullscreen></iframe>", videoTitle: "Colors")
        
        partyRocks.append(p1)
        partyRocks.append(p2)
        partyRocks.append(p3)
        partyRocks.append(p4)
        partyRocks.append(p5)
        
        tableView.delegate = self;
        tableView.dataSource = self;
    }
    
    //Recycling the cells
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PartyCell", for: indexPath) as? PartyCell {
            let partyRock = partyRocks[indexPath.row]
            
            cell.updateUI(partyRock: partyRock)
            
            return cell
        } else {
            return UITableViewCell()
        }
    }

    //number of rows in the table.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return partyRocks.count;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let partyRock = partyRocks[indexPath.row]
        performSegue(withIdentifier: "VideoVC", sender: partyRock)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destination = segue.destination as? VideoVC {
            if let party = sender as? PartyRock {
                destination.partyRock = party
            }
        }
    }
    
}
