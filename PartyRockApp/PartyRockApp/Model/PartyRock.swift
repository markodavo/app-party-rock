//
//  PartyRock.swift
//  PartyRockApp
//
//  Created by Mark Davidson on 29/8/17.
//  Copyright © 2017 Mark Davidson. All rights reserved.
//

import Foundation


class PartyRock {
    private var _imageURL: String!
    private var _videoURL: String!
    private var _videoTitle: String!
    //Getters
    var imageURL: String {
        return _imageURL
    }
    var videoURL: String {
        return _videoURL
    }
    var videoTitle: String {
        return _videoTitle
    }
    
    //Contructor
    init(imageURL: String, videoURL: String, videoTitle: String) {
        _imageURL = imageURL;
        _videoURL = videoURL;
        _videoTitle = videoTitle;
    }
    
    
}
